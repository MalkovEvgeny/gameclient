package ru.inno.game.client.socket;

import javafx.application.Platform;
import ru.inno.game.client.controllers.MainController;
import ru.inno.game.client.utils.GameUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class SocketClient extends Thread {
    private Socket socket;
    private PrintWriter toServer;
    private BufferedReader fromServer;
    private MainController controller;
    private GameUtils gameUtils;
    private boolean inGame = true;

    public SocketClient(MainController controller, String host, int port) {
        try {
            socket = new Socket(host, port);
            toServer = new PrintWriter(socket.getOutputStream(), true);
            fromServer = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            this.controller = controller;
            this.gameUtils = controller.getGameUtils();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void run() {
        while (inGame) {
            String messageFromServer;
            try{
                messageFromServer = fromServer.readLine();
                if (messageFromServer != null) {
                    switch (messageFromServer) {
                        case "left":
                            gameUtils.goLeft(controller.getEnemy());
                            break;
                        case "right":
                            gameUtils.goRight(controller.getEnemy());
                            break;
                        case "shot":
                            Platform.runLater(() -> gameUtils.createBulletFor(controller.getEnemy(), true));
                            break;
                        case "statistic":
                            readAndSendStatistic();
                            inGame = false;
                            break;
                    }
                }
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
        }
    }

    private void readAndSendStatistic() throws IOException {
        String messageFromServer;
        messageFromServer = fromServer.readLine();
        while (messageFromServer != null) {
            String finalMessageFromServer = messageFromServer;
            Platform.runLater(() -> controller.getFinishStatistic().setText(controller.getFinishStatistic().getText() + finalMessageFromServer + "\n"));
            messageFromServer = fromServer.readLine();
        }
    }

    public void sendMessage(String message) {
        toServer.println(message);
    }
}
