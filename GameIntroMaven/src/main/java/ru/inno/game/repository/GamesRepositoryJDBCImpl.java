package ru.inno.game.repository;

import ru.inno.game.models.Game;
import ru.inno.game.models.Player;

import javax.sql.DataSource;
import java.sql.*;
import java.time.Duration;
import java.time.LocalDateTime;


public class GamesRepositoryJDBCImpl implements GamesRepository {
    //  language=SQL
    private static final String SQL_FIND_BY_ID = "select * from game where id = ?";

    // language=SQL
    static private final String SQL_INSERT_GAME = "insert into " +
            "game(datetime, player_first, player_second, player_first_shots_count, player_second_shots_count, " +
            "seconds_game_time_amount) values (?, ?, ?, ?, ?, ?)";
    // language=SQL
    static private final String SQL_UPDATE_GAME = "update game set " +
            "datetime = ?, player_first = ?, player_second = ?, player_first_shots_count = ?, " +
            "player_second_shots_count = ?, seconds_game_time_amount = ? where id = ?";
    // language=SQL
    private static final String SQL_ADD_PLAYER_BY_ID = "select * from" +
            " player where id = ?";
    private DataSource dataSource;
    public GamesRepositoryJDBCImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public Game findById(Long gameId) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_FIND_BY_ID);) {
            statement.setLong(1, gameId);
            ResultSet rows = statement.executeQuery();

            if(rows.next()){
                Game game = Game.builder()
                        .id(rows.getLong("id"))
                        .dateTime(LocalDateTime.parse(rows.getString("datetime")))
                        .playerFirst(Player.builder()
                                .id(rows.getLong("player_first"))
                                .build())
                        .playerSecond(Player.builder()
                                .id(rows.getLong("player_second"))
                                .build())
                        .playerFirstShotsCount(rows.getInt("player_first_shots_count"))
                        .playerSecondShotsCount(rows.getInt("player_second_shots_count"))
                        .secondsGameTimeAmount(rows.getLong("seconds_game_time_amount"))
                        .build();

                    game.setPlayerFirst(findPlayerByIdForGame(game.getPlayerFirst().getId()));
                    game.setPlayerSecond(findPlayerByIdForGame(game.getPlayerSecond().getId()));

                return game;
            }
            return null;
        } catch (SQLException e){
            throw new IllegalArgumentException(e);
        }
    }

    public Player findPlayerByIdForGame(Long playerId){
        try (Connection connectionPlayer = dataSource.getConnection();
             PreparedStatement statementPlayer = connectionPlayer.prepareStatement(SQL_ADD_PLAYER_BY_ID);) {
            statementPlayer.setLong(1, playerId);
            ResultSet row = statementPlayer.executeQuery();

            if(row.next()){
                return Player.builder()
                         .id(row.getLong("id"))
                         .ip(row.getString("ip"))
                         .name(row.getString("name"))
                         .points(row.getInt("points"))
                         .maxWinsCount(row.getInt("max_wins_count"))
                         .maxLosesCount(row.getInt("max_loses_count"))
                         .build();
            }
            return null;

        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }


    @Override
    public void update(Game game){
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_GAME);) {
            statement.setString(1, String.valueOf(game.getDateTime()));
            statement.setLong(2, game.getPlayerFirst().getId());
            statement.setLong(3, game.getPlayerSecond().getId());
            statement.setInt(4, game.getPlayerFirstShotsCount());
            statement.setInt(5, game.getPlayerSecondShotsCount());
            statement.setLong(6, game.getSecondsGameTimeAmount());

            statement.setLong(7, game.getId());

            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) {
                throw new SQLException("Can't insert data");
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }
    @Override
    public void save(Game game) {
        try(Connection connection = dataSource.getConnection();
        PreparedStatement statement = connection.prepareStatement(SQL_INSERT_GAME,
                Statement.RETURN_GENERATED_KEYS);) {
            statement.setString(1, game.getDateTime().toString());
            statement.setLong(2, game.getPlayerFirst().getId());
            statement.setLong(3, game.getPlayerSecond().getId());
            statement.setInt(4, game.getPlayerFirstShotsCount());
            statement.setInt(5, game.getPlayerSecondShotsCount());
            statement.setLong(6, game.getSecondsGameTimeAmount());

            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1){
                throw new SQLException("Can't insert game data");
            }

            ResultSet generatedId = statement.getGeneratedKeys();

            if (generatedId.next()){
                game.setId(generatedId.getLong("id"));
            } else{
                throw new SQLException("Can't retrieve id");
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }



}
