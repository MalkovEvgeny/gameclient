package ru.inno.game.repository;

import ru.inno.game.models.Player;

import javax.sql.DataSource;
import java.sql.*;

public class PlayersRepositoryJDBCImpl implements PlayersRepository {
    // language=SQL
    private static final String SQL_SAVE_PLAYER = "insert into" +
            " player(ip, name, points, max_wins_count, max_loses_count) values (?, ?, ?, ?, ?)";
    // language=SQL
    private static final String SQL_FIND_BY_NICKNAME = "select * from" +
            " player where name = ?";
    // language=SQL
    private static final String SQL_PLAYER_UPDATE = "update player set" +
            " ip = ?, name = ?, points = ?, max_wins_count = ?, max_loses_count = ? where id = ?";



    private DataSource dataSource;

    public PlayersRepositoryJDBCImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void update(Player player) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_PLAYER_UPDATE);) {
            statement.setString(1, player.getIp());
            statement.setString(2, player.getName());
            statement.setInt(3, player.getPoints());
            statement.setInt(4, player.getMaxWinsCount());
            statement.setInt(5, player.getMaxLosesCount());
            statement.setLong(6, player.getId());

            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) {
                throw new SQLException("Can't insert data");
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }



    @Override
    public Player findByNickname(String nickname) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_FIND_BY_NICKNAME);) {
            statement.setString(1, nickname);

            ResultSet rows = statement.executeQuery();

            if (rows.next()) {
                return Player.builder()
                        .id(rows.getLong("id"))
                        .ip(rows.getString("ip"))
                        .name(rows.getString("name"))
                        .points(rows.getInt("points"))
                        .maxWinsCount(rows.getInt("max_wins_count"))
                        .maxLosesCount(rows.getInt("max_loses_count"))
                        .build();
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
        return null;
    }

    @Override
    public void save(Player player) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_SAVE_PLAYER,
                     Statement.RETURN_GENERATED_KEYS);) {
            statement.setString(1, player.getIp());
            statement.setString(2, player.getName());
            statement.setInt(3, 0);
            statement.setInt(4, 0);
            statement.setInt(5, 0);

            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) {
                throw new SQLException("Can't insert data");
            }

            ResultSet generatedId = statement.getGeneratedKeys();

            if (generatedId.next()) {
                player.setId(generatedId.getLong("id"));
            } else {
                throw new SQLException("Can't retrieve id");
            }

        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }


}
