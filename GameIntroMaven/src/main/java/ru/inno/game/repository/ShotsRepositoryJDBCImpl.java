package ru.inno.game.repository;

import ru.inno.game.models.Shot;

import javax.sql.DataSource;
import java.sql.*;
import java.time.LocalDateTime;

public class ShotsRepositoryJDBCImpl implements ShotsRepository {
    private DataSource dataSource;
    // language=SQL
    static private final String SQL_INSERT_SHOT = "insert into " +
            "shot(datetime, game, shooter, target) values (?, ?, ?, ?)";
    public ShotsRepositoryJDBCImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void save(Shot shot) {
        ResultSet generatedId = null;
        try(Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(SQL_INSERT_SHOT,
                    Statement.RETURN_GENERATED_KEYS);){
            statement.setString(1, String.valueOf(shot.getDateTime()));
            statement.setLong(2, shot.getGame().getId());
            statement.setLong(3, shot.getShooter().getId());
            statement.setLong(4, shot.getTarget().getId());

            int affectedRows = statement.executeUpdate();
            if (affectedRows != 1){
                throw new SQLException("Can't insert shot data");
            }

            generatedId = statement.getGeneratedKeys();

            if (generatedId.next()){
                shot.setId(generatedId.getLong("id"));
            }else{
                throw new SQLException("Can't retrieve id");
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }
}
