package ru.inno.game.app;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import ru.inno.game.dto.StatisticDto;
import ru.inno.game.repository.*;
import ru.inno.game.services.GameService;
import ru.inno.game.services.GameServiceImpl;
import ru.inno.game.utils.CustomDataSource;

import javax.sql.DataSource;
import java.util.Random;
import java.util.Scanner;


public class Main {
    private static final String JDBC_URL = "jdbc:postgresql://localhost:5432/postgres";
    private static final String JDBC_USER = "postgres";
    private static final String JDBC_PASSWORD = "nt7kaspy";

    public static void main(String[] args) {
        HikariConfig configuration = new HikariConfig();
        configuration.setJdbcUrl(JDBC_URL);
        configuration.setUsername(JDBC_USER);
        configuration.setPassword(JDBC_PASSWORD);
        configuration.setMaximumPoolSize(20);
        DataSource dataSource = new HikariDataSource(configuration);

        PlayersRepository playersRepository = new PlayersRepositoryJDBCImpl(dataSource);
        GamesRepository gamesRepository = new GamesRepositoryJDBCImpl(dataSource);
        ShotsRepository shotsRepository = new ShotsRepositoryJDBCImpl(dataSource);
        GameService gameService = new GameServiceImpl(playersRepository, gamesRepository, shotsRepository);


        int j = 0;

        while (j < 3) {
            System.out.println("Введите имена для игроков:");
            Scanner scanner = new Scanner(System.in);
            String first = scanner.nextLine();
            String second = scanner.nextLine();
            while (first.equals(second)){
                System.out.println("Игрок с именем: " + second + " уже выбран");
                System.out.println("Введите другое имя:");
                second = scanner.nextLine();
            }

            Random random = new Random();

            Long gameId = gameService.startGame("127.0.0.1", "127.0.0.2", first, second);

            String shooter = first;
            String target = second;
            int i = 0;

            while (i < 5) {
                System.out.println(shooter + " делайте выстрел в " + target);
                scanner.nextLine();

                int success = random.nextInt(2);

                if (success == 0) {
                    System.out.println("Успешно!");
                    gameService.shot(gameId, shooter, target);
                } else {
                    System.out.println("Промах");
                }
                String temp = shooter;
                shooter = target;
                target = temp;

                i++;

                System.out.println();
            }
            StatisticDto statistic = gameService.finishGame(gameId, System.currentTimeMillis() / 1000);
            System.out.println(statistic);
            j++;
        }
    }
}

