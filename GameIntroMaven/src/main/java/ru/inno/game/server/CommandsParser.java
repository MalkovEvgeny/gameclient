package ru.inno.game.server;

public class CommandsParser {

    public static boolean isMessageForFinishGame(String messageFromClient){
        return messageFromClient.equals("FINISH GAME");
    }

    public static boolean isMessageForShot(String messageFromClient) {
        return messageFromClient.equals("shot");
    }

    public static boolean isMessageForExit(String messageFromClient) {
        return messageFromClient.equals("exit");
    }

    public static boolean isMessageForMove(String messageFromClient) {
        return messageFromClient.equals("left") || messageFromClient.equals("right");
    }

    public static boolean isMessageForNickname(String messageFromClient) {
        return messageFromClient.startsWith("name: ");
    }


    public static boolean isMessageForDamage(String messageFromClient) {
        return messageFromClient.equals("DAMAGE");
    }
}
