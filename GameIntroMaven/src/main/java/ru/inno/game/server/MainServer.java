package ru.inno.game.server;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import ru.inno.game.repository.*;
import ru.inno.game.services.GameService;
import ru.inno.game.services.GameServiceImpl;

import javax.sql.DataSource;

public class MainServer {
    private static final String JDBC_URL = "jdbc:postgresql://localhost:5432/postgres";
    private static final String JDBC_USER = "postgres";
    private static final String JDBC_PASSWORD = "nt7kaspy";

    public static void main(String[] args) {
        HikariConfig configuration = new HikariConfig();
        configuration.setJdbcUrl(JDBC_URL);
        configuration.setUsername(JDBC_USER);
        configuration.setPassword(JDBC_PASSWORD);
        configuration.setMaximumPoolSize(20);
        DataSource dataSource = new HikariDataSource(configuration);
        GamesRepository gamesRepository = new GamesRepositoryJDBCImpl(dataSource);
        PlayersRepository playersRepository = new PlayersRepositoryJDBCImpl(dataSource);
        ShotsRepository shotsRepository = new ShotsRepositoryJDBCImpl(dataSource);
        GameService gameService = new GameServiceImpl(playersRepository, gamesRepository, shotsRepository);

        GameServer gameServer = new GameServer(gameService);
        gameServer.start(7777);


    }
}
