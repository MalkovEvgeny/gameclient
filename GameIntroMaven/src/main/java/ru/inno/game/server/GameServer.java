package ru.inno.game.server;

import ru.inno.game.dto.StatisticDto;
import ru.inno.game.services.GameService;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import static ru.inno.game.server.CommandsParser.*;

public class GameServer {
    private ClientThread firstPlayer;
    private ClientThread secondPlayer;
    private GameService gameService;
    private boolean isGameStarted = false;
    private Lock lock = new ReentrantLock();
    private boolean isGameInProcess = true;
    private long gameId;
    private long startTimeMills;
    private StatisticDto statisticDto;

    private ServerSocket serverSocket;

    public GameServer(GameService gameService) {
        this.gameService = gameService;
    }

    public void start(int port) {
        try {
            serverSocket = new ServerSocket(port);
            System.out.println("СЕРВЕР ЗАПУЩЕН...");
            System.out.println("ОЖИДАНИЕ ПОДКЛЮЧЕНИЯ ПЕРВОГО КЛИЕНТА...");
            firstPlayer = connect();
            System.out.println("ОЖИДАНИЕ ПОДКЛЮЧЕНИЯ ВТОРОГО КЛИЕНТА...");
            secondPlayer = connect();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    private ClientThread connect() {
        Socket client;
        try {
            client = serverSocket.accept();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
        ClientThread clientThread = new ClientThread(client);
        clientThread.start();
        System.out.println("КЛИЕНТ ПОДКЛЮЧЕН");
        clientThread.sendMessage("Вы подключены в серверу!");
        return clientThread;
    }

    private class ClientThread extends Thread {
        private PrintWriter toClient;
        private BufferedReader fromClient;
        private String playerNickname;
        private String ip;

        public ClientThread(Socket client) {
            try {
                this.toClient = new PrintWriter(client.getOutputStream(), true);
                this.fromClient = new BufferedReader(new InputStreamReader(client.getInputStream()));
                this.ip = client.getInetAddress().getHostAddress();
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
        }

        @Override
        public void run() {
            while (isGameInProcess) {
                String messageFromClient;
                try {
                    messageFromClient = fromClient.readLine();
                } catch (IOException e) {
                    throw new IllegalStateException(e);
                }
                if (messageFromClient != null) {
                    if (isMessageForNickname(messageFromClient)) {
                        resolveNickname(messageFromClient);
                    } else if (isMessageForExit(messageFromClient) && isGameInProcess) {
                        resolveExit();
                    } else if (isMessageForMove(messageFromClient)) {
                        resolveMove(messageFromClient);
                    } else if (isMessageForShot(messageFromClient)) {
                        resolveShot(messageFromClient);
                    } else if (isMessageForDamage(messageFromClient)) {
                        resolveDamage();
                    } else if (isMessageForFinishGame(messageFromClient)) {
                        resolveFinish();
                    }
                }


                lock.lock();
                if (isReadyStartGame()) {
                    gameId = gameService.startGame(firstPlayer.getIp(), secondPlayer.getIp(), firstPlayer.playerNickname, secondPlayer.playerNickname);
                    startTimeMills = System.currentTimeMillis();
                    isGameStarted = true;
                }
                lock.unlock();
            }
        }

        private void resolveDamage() {
            if (meFirst()) {
                gameService.shot(gameId, firstPlayer.playerNickname, secondPlayer.playerNickname);
            } else {
                gameService.shot(gameId, secondPlayer.playerNickname, firstPlayer.playerNickname);
            }
        }


        private void resolveShot(String messageFromClient) {
            if (meFirst()) {
                secondPlayer.sendMessage(messageFromClient);
            } else {
                firstPlayer.sendMessage(messageFromClient);
            }
        }

        private void resolveMove(String messageFromClient) {
            if (meFirst()) {
                secondPlayer.sendMessage(messageFromClient);
            } else {
                firstPlayer.sendMessage(messageFromClient);
            }
        }

        private void resolveFinish() {
            if (meFirst() && isGameInProcess) {
                statisticDto  = gameService.finishGame(gameId, (System.currentTimeMillis() - startTimeMills) / 1000);
                sendMessage("statistic");
                sendMessage(statisticDto.toString());
                secondPlayer.sendMessage("statistic");
                secondPlayer.sendMessage(statisticDto.toString());
                isGameInProcess = false;
            }
        }

        private void resolveExit() {
            lock.lock();
            gameService.finishGame(gameId, (System.currentTimeMillis() - startTimeMills) / 1000);
            isGameInProcess = false;
            lock.unlock();
        }

        private void resolveNickname(String messageFromClient) {
            if (meFirst()) {
                firstPlayer.playerNickname = messageFromClient.substring(6);
                System.out.println("ИМЯ ПЕРВОГО ИГРОКА: " + messageFromClient);
                secondPlayer.sendMessage(messageFromClient);
            } else {
                secondPlayer.playerNickname = messageFromClient.substring(6);
                System.out.println("ИМЯ ВТОРОГО ИГРОКА: " + messageFromClient);
                firstPlayer.sendMessage(messageFromClient);
            }
        }

        public void sendMessage(String message) {
            toClient.println(message);
        }

        private boolean meFirst() {
            return this == firstPlayer;
        }

        public String getIp() {
            return ip;
        }

        private boolean isReadyStartGame() {
            return firstPlayer.playerNickname != null && secondPlayer.playerNickname != null && !isGameStarted;
        }


    }
}
